set term svg
set output ARG2
unset key
set dgrid3d 65, 65
set xlabel 'x'
set ylabel 'y'
set zlabel 'φ'
set xrange [-3.3:3.3]
set yrange [-3.3:3.3]
splot ARG1 u 3:4:7 w lines
