set term svg
set output ARG2
unset key
set xlabel 'x'
set ylabel 'Δφ/φ_c'
plot ARG1 u 2:7 w linespoints
