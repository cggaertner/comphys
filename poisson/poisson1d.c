#include <math.h>
#include <stdio.h>

#define N 10
#define PHI_A 0
#define PHI_B 100
#define EPSILON 1e-5

static double lattice[2][N];

static double step(double *phi_0, double *phi)
{
    double D = 0;

    for(int i = 1; i < N-1; ++i) {
        phi[i] = (phi_0[i-1] + phi_0[i+1]) / 2;
        double d = fabs(phi[i] - phi_0[i]);
        if(d > D) D = d;
    }

    return D;
}

static void dump(double *phi)
{
    for(int i = 0; i < N; ++i)
        printf("%.1f ", phi[i]);

    putchar('\n');
}

int main(void)
{
    lattice[0][0] = lattice[1][0] = PHI_A;
    lattice[0][N-1] = lattice[1][N-1] = PHI_B;

    dump(lattice[0]);

    _Bool flip = 0;
    while(step(lattice[flip], lattice[!flip]) > EPSILON) {
        flip = !flip;
        dump(lattice[flip]);
    }

    return 0;
}
