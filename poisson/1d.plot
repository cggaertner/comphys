set term svg
set output '1d.svg'
set title 'converging potential'
unset key
set ylabel 'φ'
set xlabel 'k'
plot 100/9*x, for [i=1:10] '1d.out' u (i-1):(column(i)) w points pt 8 lc rgb 'red'
