// lattice size
#define N 65

// iteration termination condition
#define DELTA 100.0

// charge
#define q 1.0

// lattice spacing
#define dx 0.1

// electric constant
#define eps_0 8.854187817620e-12

// charge placement
#ifdef ASYM
#define i_0 8
#define j_0 8
#define k_0 32
#else
#define i_0 32
#define j_0 32
#define k_0 32
#endif

// boundary conditions
#ifdef BOXED
#define COMPUTE_COULOMB_BOUNDARY 0
#else
#define COMPUTE_COULOMB_BOUNDARY 1
#endif
