#include <math.h>
#include <stdio.h>
#include <stdlib.h>

#include "parameters.h"

static const double q_c = q / (4 * M_PI * eps_0),
                    q_l = q / (eps_0 * dx);

static double phi_coulomb[N][N][N],
              phi_lattice[2][N][N][N],
              dphi_rel[N][N][N];

static double iterate(double phi_0[N][N][N], double phi[N][N][N])
{
    double D = 0;

    for(int i = 1; i < N-1; ++i)
    for(int j = 1; j < N-1; ++j)
    for(int k = 1; k < N-1; ++k) {
        phi[i][j][k] = (
                (i == i_0 && j == j_0 && k == k_0) * q_l
                + phi_0[i-1][j][k] + phi_0[i+1][j][k]
                + phi_0[i][j-1][k] + phi_0[i][j+1][k]
                + phi_0[i][j][k-1] + phi_0[i][j][k+1]
            ) / 6;

        double d = fabs(phi[i][j][k] - phi_0[i][j][k]);
        if(d > D) D = d;
    }

    return D;
}

static void dump(double phi[N][N][N])
{
    puts("# k_rel j_rel i_rel x y z phi phi_coulomb dphi_rel");

    for(int i = 0; i < N; ++i)
    for(int j = 0; j < N; ++j)
    for(int k = 0; k < N; ++k) {
        int i_rel = i - i_0,
            j_rel = j - j_0,
            k_rel = k - k_0;

        double x = (i - N/2) * dx,
               y = (j - N/2) * dx,
               z = (k - N/2) * dx;

        printf("%i %i %i %g %g %g %g %g %g\n",
            k_rel, j_rel, i_rel, x, y, z,
            phi[i][j][k], phi_coulomb[i][j][k], dphi_rel[i][j][k]);
    }
}

int main(int argc, char *argv[])
{
    (void)argc;

    // shorten program name
    char *name = argv[0];
    for(char *cc = name; *cc; ++cc) {
        if(*cc == '/' || *cc == '\\')
            name = cc + 1;
    }

    // compute coulomb potential
    for(int i = 0; i < N; ++i)
    for(int j = 0; j < N; ++j)
    for(int k = 0; k < N; ++k) {
        double x = (i - i_0) * dx,
               y = (j - j_0) * dx,
               z = (k - k_0) * dx;

        phi_coulomb[i][j][k] = q_c / sqrt(x*x + y*y + z*z);

#if COMPUTE_COULOMB_BOUNDARY
        if(i == 0 || j == 0 || k == 0 || i == N-1 || j == N-1 || k == N-1)
            phi_lattice[0][i][j][k] = phi_lattice[1][i][j][k]
                                    = phi_coulomb[i][j][k];
#endif
    }

    // compute lattice potential
    unsigned steps = 1, flip = 0;
    double delta;

    while((delta = iterate(phi_lattice[flip], phi_lattice[!flip])) > DELTA) {
        flip = !flip;
        ++steps;
    }

    // compute dphi_rel
    for(int i = 0; i < N; ++i)
    for(int j = 0; j < N; ++j)
    for(int k = 0; k < N; ++k) {
        dphi_rel[i][j][k] = (phi_lattice[flip][i][j][k] - phi_coulomb[i][j][k])
                            / phi_coulomb[i][j][k];
    }

    // fix singularities
    phi_coulomb[i_0][j_0][k_0] = phi_lattice[flip][i_0][j_0][k_0];
    dphi_rel[i_0][j_0][k_0] = (dphi_rel[i_0-1][j_0][k_0]
                             + dphi_rel[i_0+1][j_0][k_0]
                             + dphi_rel[i_0][j_0-1][k_0]
                             + dphi_rel[i_0][j_0+1][k_0]
                             + dphi_rel[i_0][j_0][k_0-1]
                             + dphi_rel[i_0][j_0][k_0+1]) / 6;

    dump(phi_lattice[flip]);
    fprintf(stderr, "%s: delta=%.4g, steps=%u\n", name, delta, steps);

    return 0;
}
