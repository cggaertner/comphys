make targets:
  all        make all projects [default target]
  bin        build executables only
  tar        create particles.tar.gz
  clean      remove some temporary files
  realclean  remove all generated files
  pull       pull from git remote
  <project>  make single project

note:
  on linux, LDLIBS=-lm needs to be set manually
