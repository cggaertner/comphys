ifeq ($(origin CC),default)
CC = clang
CFLAGS ?= -O3
endif

PROJECTS = tempconv decay randomwalk poisson particles
TAR_FILES = $(wildcard particles/README.* particles/slides/* particles/*.svg\
              particles/Makefile particles/*.[ch] particles/*.in particles/*.plot)
ACTION =

export CC CFLAGS LDFLAGS LDLIBS

all: $(PROJECTS)

bin: ACTION = bin
bin: $(PROJECTS)

tar: particles particles.tar.gz

clean: ACTION = clean
clean: $(PROJECTS)

realclean: ACTION = realclean
realclean: $(PROJECTS)

pull:
	git pull
	git submodule update --init --remote

.PHONY: $(PROJECTS)
$(PROJECTS):
	$(MAKE) -C $@ $(ACTION)

particles.tar.gz: $(TAR_FILES) | particles
	tar -czf $@ $^
