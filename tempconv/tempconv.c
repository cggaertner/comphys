#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MAX_LINE 64

int main(int argc, char *argv[])
{
    if(argc != 2) {
        puts(
            "Usage: tempconv <FILE>\n"
            "Format: <VALUE>[°]<F|C>\n"
            "        12.5F\n"
            "        -5°C");

        return 1;
    }

    FILE *fp = fopen(argv[1], "r");
    if(!fp) {
        perror("io error: ");
        return 1;
    }

    static char line[MAX_LINE];
    unsigned n = 0;
    while(fgets(line, sizeof line, fp)) {
        ++n;
        if(*line == '\n')
            continue;

        char *lf = strchr(line, '\n');
        if(lf) *lf = 0;

        char *tail;
        double value = strtod(line, &tail);
        if(tail == line) {
            fprintf(stderr, "[%u] non-numeric input: '%s'\n", n, line);
            return 1;
        }

        size_t len = strlen("°");
        if(strncmp(tail, "°", len) == 0)
            tail += len;

        switch(*tail) {
            case 'f':
            case 'F':
            printf("%10.2f°F = %10.2f°C\n", value, (value - 32) * 5. / 9);
            break;

            case 'c':
            case 'C':
            printf("%10.2f°C = %10.2f°F\n", value, value * 1.8 + 32);
            break;

            default:
            fprintf(stderr,
                    "[%u] input with missing or incorrect unit: %s\n",
                    n, line);
            return 1;
        }

        if(tail[1] && !isspace(tail[1])) {
            fprintf(stderr,
                    "[%u] ignored superfluous characters in unit '%s'\n",
                    n, tail);
        }
    }

    if(ferror(fp)) {
        perror("io error: ");
        return 1;
    }
}
