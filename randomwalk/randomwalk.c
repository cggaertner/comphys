#include <stdio.h>
#include <stdlib.h>

#define SEED 42

#define MAX_PARTICLES (1 << 20)
#define MAX_STEPS (1 << 17)

static long x_of_particle[MAX_PARTICLES];
static long long sum_sqx_of_time[MAX_STEPS + 1];
static long n_of_x[MAX_STEPS * 2 + 1];

static _Bool random_bit(void)
{
    // low bits might be badly distributed
    return rand() & 1 << 12;
}

int main(int argc, char *argv[])
{
    srand(SEED);

    if(argc != 3) {
        fputs("Usage: randomwalk <PARTICLES> <STEPS>\n", stderr);
        return 1;
    }

    long count = strtol(argv[1], NULL, 10);
    if(count <= 0) {
        fprintf(stderr, "illegal particle count %s\n", argv[1]);
        return 1;
    }
    if(count > MAX_PARTICLES) {
        fprintf(stderr, "cannot simulate more than %u particles\n",
            MAX_PARTICLES);
        return 1;
    }

    long steps = strtol(argv[2], NULL, 10);
    if(steps <= 0) {
        fprintf(stderr, "illegal step count %s\n", argv[2]);
        return 1;
    }
    if(steps > MAX_STEPS) {
        fprintf(stderr, "cannot simulate more than %u steps\n", MAX_STEPS);
        return 1;
    }

    for(long p = 0; p < count; ++p) {
        for(long t = 1; t <= steps; ++t) {
            x_of_particle[p] += 1 - 2 * random_bit();
            sum_sqx_of_time[t] += x_of_particle[p] * x_of_particle[p];
        }
    }

    FILE *variance = fopen("variance.out", "w");
    if(!variance) {
        perror("variance.out");
        return 1;
    }

    for(long t = 0; t <= steps; ++t)
        fprintf(variance, "%lu %f\n", t, (double)sum_sqx_of_time[t] / count);

    fclose(variance);

    FILE *final = fopen("final.out", "w");
    if(!final) {
        perror("final.out");
        return 1;
    }

    for(long p = 0; p < count; ++p)
        ++n_of_x[x_of_particle[p] + steps];

    for(long i = 0; i < steps * 2 + 1; ++i) {
        long x = i - steps;
        if(n_of_x[i])
            fprintf(final, "%li %lu\n", x, n_of_x[i]);
    }

    fclose(final);

    return 0;
}
