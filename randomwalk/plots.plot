set term svg

N = ARG1
T = ARG2

v(t) = 2*D*t
fit v(x) 'variance.out' via D

set output 'variance.svg'
set xlabel 't'
set ylabel '<x²>'
plot 'variance.out' t 'data points', \
    v(x) t sprintf('linear fit (D=%f)', D)

n(x) = N/sqrt(pi*D*T) * exp(-x*x/(4*D*T))

set output 'final.svg'
set xlabel 'x'
set ylabel 'n'
plot 'final.out' t 'data points', n(x) t sprintf('gaussian (D=%f)', D)
