OUTPUT = ARG1
INPUT = ARG2
N = ARG3
L = ARG4

n(x) = N*exp(-l*x)
fit log(n(x)) INPUT u (column(0)):(log(column(1))) via l

set term svg
set output OUTPUT
set xlabel 'k'
set ylabel 'N_k'
set logscale y
plot INPUT t sprintf('discrete data generated with λ = %s', L), \
    n(x) t sprintf('exponential fit with λ = %f', l)
