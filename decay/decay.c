#include "tinymt64.h"
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define SEED 42

int main(int argc, char *argv[])
{
    if(argc != 3) {
        fputs("Usage: decay <N> <lambda>\n", stderr);
        return 1;
    }

    long long N = strtoll(argv[1], NULL, 10);
    if(!N) {
        fprintf(stderr, "illegal argument N = %s\n", argv[1]);
        return 1;
    }

    double lambda = strtod(argv[2], NULL);
    if(!(_Bool)lambda) {
        fprintf(stderr, "illegal argument lambda = %s\n", argv[2]);
        return 1;
    }

    if(N <= 0) {
        fputs("argument N has to be positive\n", stderr);
        return 1;
    }

    if(lambda <= 0.0 || lambda >= 1.0) {
        fputs("argument lambda must be in interval (0,1)\n", stderr);
        return 1;
    }

    uint64_t state[2];
    tinymt64_init(state, SEED);

    printf("%lli\n", N);
    while(N) {
        long long i = N;
        while(i--) {
            double r = tinymt64_generate_double(state);
            if(r <= lambda)
                --N;
        }

        printf("%lli\n", N);
    }

    return 0;
}
